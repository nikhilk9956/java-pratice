
public class intro {

	public static void main(String[] args) {
		int arr[] = {-6,-4,-2,0,2,4,6,8,10,5};
		int target  = 0;
		int ans  = binarySearch(arr, target);
		System.out.println(ans);
		
	}
	
	// return the index
	// return -1 it does not exit 
	
	static int binarySearch(int[] arr,int target) {
		int start = 0;
		int end  = arr.length-1;
		
		while(start <= end ) {
			//find the middle element 
// int mid =  (start + end)/2; // might be posible that (start + end ) exceed the range  of int in java 
			int mid = start + (end - start)/2;
			if (target < arr[mid]) {
				end =  mid -1;
			}else if (target>arr[mid]) {
				start = mid +1;
			}else {
				return mid;
			}
		}
		return -1;
		
		
		
	}

}
